from django.db import models


# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    jumlahsks = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=100)
    semestertahun = models.CharField(max_length=100)
    ruangan = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
